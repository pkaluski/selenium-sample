package com.testelence.automation;

import org.testng.annotations.*;
import org.testng.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.*;
import org.openqa.selenium.Keys;
import java.io.*; 

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    private WebDriver driver = null;
    private String email = "";
    private int nbr = 0;

    /*
     * Since you cannot add the same user twice, the user name has
     * to be different on each run. It is done by storing the
     * number used is the last test and then retrieving it.
     * The email used is pkaluski<number>@piotrkaluski.com
     */
    @BeforeSuite
    public void loadMailNbr()
    {
        try
        {
            File file = new File("param.txt"); 
  
            BufferedReader br = new BufferedReader(new FileReader(file)); 
  
            String st = br.readLine(); 
            nbr = Integer.parseInt( st );
            nbr++;
            email = "pkaluski" + nbr + "@piotrkaluski.com";

        }catch( Exception e ){
            System.out.println( "Cannot open param file" );
            System.out.println( e );
            Assert.fail("Cannot open param file. Please create param.txt file");
        }

    }

    @AfterSuite
    public void saveMailNbr()
    {
        try
        {
            PrintWriter out = new PrintWriter( "param.txt" );
            out.println( nbr );
            out.close();

        }catch( Exception e ){
            System.out.println( "Cannot open param file" );
            System.out.println( e );
            Assert.fail("Cannot open param file");
        }
    }

    @BeforeMethod
    public void setUp()
    {
        driver = new FirefoxDriver();
            
    }

    @AfterMethod
    public void tearDown()
    {
        driver.quit();
    }

    @Test
    public void createAccount()
    {
        driver.get( "http://automationpractice.com/index.php" );
        driver.findElement( By.xpath( "//a[@title='Log in to your customer account']") ).click();
        driver.findElement( By.id( "email_create" ) ).sendKeys( email );
        driver.findElement( By.id( "SubmitCreate" ) ).click();
        WebDriverWait wait = new WebDriverWait( driver, 10 );
        wait.until( ExpectedConditions.presenceOfElementLocated( By.id("id_gender1") ) );
        driver.findElement( By.id( "id_gender1" ) ).click();
        driver.findElement( By.id( "customer_firstname" ) ).sendKeys("Piotr");
        driver.findElement( By.id( "customer_lastname" ) ).sendKeys( "Tester" );
        driver.findElement( By.id( "passwd" ) ).sendKeys( "superpass" );
        driver.findElement( By.id( "days" ) ).sendKeys( "3" );
        driver.findElement( By.id( "months" ) ).sendKeys( "April" );
        Select years = new Select( driver.findElement( By.xpath( "//select[@id='years']" ) ) );
        years.selectByIndex( 30 );
        driver.findElement( By.id( "firstname" ) ).sendKeys("Piotr");
        driver.findElement( By.id( "lastname" ) ).sendKeys( "Tester" );
        driver.findElement( By.id( "company" ) ).sendKeys( "Tester" );
        driver.findElement( By.id( "address1" ) ).sendKeys( "Moj adres, ul. Moje 23" );
        driver.findElement( By.id( "city" ) ).sendKeys( "Testowice" );
        Select state = new Select( driver.findElement( By.xpath( "//select[@id='id_state']" ) ) );
        state.selectByVisibleText( "Alabama" );
        driver.findElement( By.id( "postcode" ) ).sendKeys( "90345" );
        driver.findElement( By.id( "other" ) ).sendKeys( "This is an additional information tekst" );
        driver.findElement( By.id( "phone_mobile" ) ).sendKeys("502290250");
        driver.findElement( By.id( "alias" ) ).sendKeys(
                 Keys.chord(Keys.SHIFT, Keys.HOME ) + "piotr.kaluski@testelence.pl");

        driver.findElement( By.id( "submitAccount" ) ).click();
        wait.until( ExpectedConditions.presenceOfElementLocated( 
                      By.xpath("//a[@title='View my customer account']/span[text()='Piotr Tester']" ) ) );

    }

    @Test (dependsOnMethods={"createAccount"})
    public void login()
    {
        driver.get( "http://automationpractice.com/index.php" );
        driver.findElement( By.xpath( "//a[@title='Log in to your customer account']") ).click();
        driver.findElement( By.id( "email") ).sendKeys(email);
        driver.findElement( By.id( "passwd") ).sendKeys("superpass");
        driver.findElement( By.id( "SubmitLogin") ).click();
        WebDriverWait wait = new WebDriverWait( driver, 10 );
        wait.until( ExpectedConditions.presenceOfElementLocated( 
                      By.xpath("//a[@title='View my customer account']/span[text()='Piotr Tester']" ) ) );
    }

    /*
     * Add a comment to a product
     */
    @Test (dependsOnMethods={"login"})
    public void comment()
    {
        driver.get( "http://automationpractice.com/index.php" );
        driver.findElement( By.xpath( "//a[@title='Log in to your customer account']") ).click();
        driver.findElement( By.id( "email") ).sendKeys(email);
        driver.findElement( By.id( "passwd") ).sendKeys("superpass");
        driver.findElement( By.id( "SubmitLogin") ).click();
        WebDriverWait wait = new WebDriverWait( driver, 10 );
        wait.until( ExpectedConditions.presenceOfElementLocated( By.xpath( "//div/ul/li/a[@title='Women']") ) );
        driver.findElement( By.xpath( "//div/ul/li/a[@title='Women']") ).click();
        driver.findElement( By.xpath( "//li/div/a[@title='Dresses']") ).click();
        driver.findElement( By.xpath( "//div[@id='center_column']/ul/li") ).click();
        driver.findElement( By.xpath( "//div[@id='product_comments_block_extra']/ul/li/a") ).click();
        driver.findElement( By.id( "comment_title") ).sendKeys("Cool stuff");
        driver.findElement( By.id( "content") ).sendKeys("A really nice dress");
        driver.findElement( By.id( "submitNewMessage") ).click();
        wait.until( ExpectedConditions.presenceOfElementLocated( By.xpath("//button/span[text()='OK']" ) ) );
        driver.findElement( By.xpath("//button/span[text()='OK']" ) ).click();
        if( driver.findElements( By.xpath( "//div[@id='product_comments_block_extra']/ul/li/a") ).size() != 0 ){
            Assert.fail("Review button is still present");
        }
    }

}
